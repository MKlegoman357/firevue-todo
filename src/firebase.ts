import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import config from "@/firebase.config";

export const firebaseApp = firebase.initializeApp(config);
export const db = firebaseApp.firestore();
export const auth = firebaseApp.auth();