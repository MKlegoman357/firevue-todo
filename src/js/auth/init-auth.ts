import {auth} from "@/firebase";

let authInitialized: boolean = false;
let initCallbacks: (() => any)[] = [];

export function isAuthInitialized() {
    return authInitialized;
}

export function onAuthInit(callback: () => any) {
    if (authInitialized) {
        callback();
        return;
    }

    initCallbacks.push(callback);
}

export default function initAuth(): Promise<void> {
    return new Promise<void>(
        (resolve) => {
            auth.onAuthStateChanged((user) => {
                if (user === null) {
                    auth.signInAnonymously();
                }
                authInitialized = true;
                resolve();
            });
        }
    ).then(() => {
        for (let callback of initCallbacks) {
            try {
                callback();
            } catch (e) {
            }
        }

        initCallbacks = undefined as any;
    });
}