import * as firebase from "firebase/app";
import {auth} from "@/firebase";

export const googleProvider = new firebase.auth.GoogleAuthProvider();

export async function initSignInWithGoogle() {
    await auth.signInWithRedirect(googleProvider).catch((error: firebase.auth.AuthError) => {
        alert(error);
    })
}