import {auth} from "@/firebase";

export function isLoggedIn() {
    return auth.currentUser !== null && !auth.currentUser.isAnonymous;
}