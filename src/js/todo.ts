import {randomString} from "@/js/utils";

export interface TodoList {
    id: string;
    title: string;
    items: TodoItem[];
}

export interface TodoItem {
    id: string;
    done: boolean;
    text: string;
}

export type PartialTodoList = {
    id?: string;
    title?: string;
    items?: PartialTodoItem[];
}

export type PartialTodoItem = string | {
    id?: string;
    done?: boolean;
    text?: string;
}

export function copyTodoItems(items: TodoItem[]): TodoItem[] {
    return items.map(item => ({
        id: item.id,
        text: item.text,
        done: item.done
    }));
}

export function copyTodoList(todoList: TodoList): TodoList {
    return {
        id: todoList.id,
        title: todoList.title,
        items: copyTodoItems(todoList.items)
    };
}

export class EditableTodoList implements TodoList {
    id: string;
    title: string;
    items: TodoItem[];

    private lastId: number = 0;

    constructor(title: string = "", items: PartialTodoItem[] = [], id: string = randomString()) {
        this.id = id;
        this.title = title;
        this.items = items.map(item => {
            if (typeof item === "string") {
                return {
                    id: this.generateNewId(),
                    text: item,
                    done: false
                };
            }

            return {
                id: item.id === undefined ? this.generateNewId() : item.id,
                text: item.text === undefined ? "" : item.text,
                done: item.done === undefined ? false : item.done
            };
        });
    }

    addItem(text: string = ""): void {
        this.items.push({
            id: this.generateNewId(),
            text: text,
            done: false
        });
    }

    removeItem(id: string): boolean;
    removeItem(item: TodoItem): boolean;
    removeItem(id: string | TodoItem): boolean {
        for (let i = 0; i < this.items.length; i++) {
            let item = this.items[i];

            if (item === id || item.id === id) {
                this.items.splice(i, 1);
                return true;
            }
        }

        return false;
    }

    merge(other: PartialTodoList) {
        if (other.id !== undefined) this.id = other.id;
        if (other.title !== undefined) this.title = other.title;

        if (other.items !== undefined) {
            for (let otherItem of other.items) {
                if (typeof otherItem === "string") {
                    this.addItem(otherItem);
                } else {
                    let otherId = otherItem.id;
                    let item = this.items.find(item => item.id === otherId);

                    if (item) {
                        if (otherItem.id !== undefined) item.id = otherItem.id;
                        if (otherItem.text !== undefined) item.text = otherItem.text;
                        if (otherItem.done !== undefined) item.done = otherItem.done;
                    } else {
                        this.items.push({
                            id: otherItem.id === undefined ? this.generateNewId() : otherItem.id,
                            text: otherItem.text === undefined ? "" : otherItem.text,
                            done: otherItem.done === undefined ? false : otherItem.done,
                        });
                    }
                }
            }
        }
    }

    toPlainObject(): TodoList {
        return copyTodoList(this);
    }

    private generateNewId(): string {
        let newId: string;

        do {
            newId = (this.lastId++).toString();
        } while (this.items && this.items.some(item => item.id === newId));

        return newId;
    }

    static from(list: PartialTodoList): EditableTodoList {
        return new EditableTodoList(list.title, list.items, list.id);
    }
}