import "reflect-metadata";

import Vue from "vue";
import "@/firebase";
import store from "@/store/store";
import router from "@/router";
import App from "@/App.vue";
import IdPlugin from "@/plugins/IdPlugin/IdPlugin";
import "@/libs/bootstrap";
import initAuth from "@/js/auth/init-auth";

/* Plugins */
Vue.use(IdPlugin);

initAuth();

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
