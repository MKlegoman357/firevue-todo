import * as todo from "@/js/todo";
import {
    addCollection,
    addDocument, deleteDocument,
    FirestoreObjectOptions, setDocument,
    subscribeToCollection,
    updateCollectionFromSnapshot
} from "@/js/firestore-utils";
import {firestore} from "firebase/app";
import {Action, Mutation, State, VuexModule} from "@/plugins/vuex/vuex-decorators/vuex-decorators";
import {StoreState} from "@/store/store";
import {copyTodoItems, copyTodoList} from "@/js/todo";

let todosFirestoreOptions: FirestoreObjectOptions = {
    collectionPath: "todos",
    subcollections: [{
        collectionPath: "items",
        query(collection) {
            return collection.orderBy("text", "desc");
        }
    }]
};

export type FirestoreModuleState = {
    todos: todo.TodoList[]
}

export default class FirestoreModule extends VuexModule<FirestoreModuleState, StoreState> {
    private firstLoad = true;
    private unsubscribe: (() => void) | null = null;

    @State
    todos: todo.TodoList[] = [];

    constructor() {
        super();
        this.initVuexModule();
    }

    @Mutation
    updateTodos(refPath: string[], snapshot: firestore.QuerySnapshot) {
        if (this.firstLoad) {
            this.todos = [];
            this.firstLoad = false;
        }

        updateCollectionFromSnapshot(this.todos, todosFirestoreOptions, refPath, snapshot);
    }

    @Action
    async toggleSyncTodos() {
        if (this.unsubscribe) {
            this.unsubscribe();
            this.unsubscribe = null;
        } else {
            this.firstLoad = true;
            this.unsubscribe = subscribeToCollection(todosFirestoreOptions, (refPath, snapshot) => {
                this.updateTodos(refPath, snapshot);
            }, (error) => {
                console.error(error);
            });
        }
    }

    async updateList(updatedList: todo.TodoList) {
        let oldList = this.todos.find(todo => todo.id === updatedList.id);

        if (oldList) {
            let updatedItems: todo.TodoItem[] = [];
            let addedItems: todo.TodoItem[] = [];
            let deletedItemsIds: string[] = oldList.items.map(item => item.id);

            for (let updatedItem of updatedList.items) {
                let oldItem = oldList.items.find(item => item.id === updatedItem.id);

                if (oldItem) {
                    updatedItems.push(updatedItem);
                    deletedItemsIds = deletedItemsIds.filter(id => id !== oldItem!.id);
                } else {
                    addedItems.push(updatedItem);
                }
            }

            let documentItemsPath = `todos/${oldList.id}/items`;

            for (let id of deletedItemsIds) {
                await deleteDocument(id, documentItemsPath);
            }

            await addCollection(copyTodoItems(addedItems), documentItemsPath);

            await setDocument(copyTodoList({
                ...updatedList,
                items: updatedItems
            }), todosFirestoreOptions);
        } else {
            return await this.addList(updatedList);
        }
    }

    async addList(newList: todo.TodoList) {
        return await addDocument(copyTodoList(newList), todosFirestoreOptions);
    }

    async deleteList(id: string) {
        return await deleteDocument(id, todosFirestoreOptions);
    }
}