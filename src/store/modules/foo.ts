import {Mutation, VuexModule} from "@/plugins/vuex/vuex-decorators/vuex-decorators";
import {StoreState} from "@/store/store";

export type FooModuleState = number;

export default class FooModule extends VuexModule<FooModuleState, StoreState> {

    state: number = 5;

    constructor() {
        super();
        this.initVuexModule();
    }

    @Mutation
    change() {
        this.state += 1;
    }

}