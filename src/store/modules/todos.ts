import * as todo from "@/js/todo";
import {Mutation, State, VuexModule} from "@/plugins/vuex/vuex-decorators/vuex-decorators";

export type TodosState = Array<todo.TodoList>;
export type UpdateListPayload = {
    id: string;
    title?: string;
    items?: {
        [index: number]: {
            id?: string;
            text?: string;
            done?: boolean;
        }
    };
};

export default class TodosModule extends VuexModule {
    @State
    lists: todo.TodoList[] = [
        new todo.EditableTodoList(
            "My first list",
            ["do this", "do that", "and then maybe this"],
            "todo1"
        ).toPlainObject()
    ];

    constructor() {
        super();
        this.initVuexModule();
    }

    @Mutation
    updateList(updatedList: UpdateListPayload) {
        for (let list of this.lists) {
            if (list.id === updatedList.id) {
                if (updatedList.title !== undefined) list.title = updatedList.title;

                if (updatedList.items !== undefined) {
                    let itemsToRemove = list.items.map(item => item);

                    for (let index in updatedList.items) {
                        let updatedItem = updatedList.items[index];
                        let item = updatedItem.id !== undefined ? list.items.find(item => item.id === updatedItem.id) : list.items[index];

                        if (item) {
                            if (updatedItem.text !== undefined) item.text = updatedItem.text;
                            if (updatedItem.done !== undefined) item.done = updatedItem.done;

                            // @ts-ignore
                            itemsToRemove = itemsToRemove.filter(itemToRemove => itemToRemove.id !== item.id);
                        } else {
                            list.items.push({
                                id: updatedItem.id === undefined ? TodosModule.generateNewId(newId => list.items.some(item => item.id === newId)) : updatedItem.id,
                                text: updatedItem.text === undefined ? "" : updatedItem.text,
                                done: updatedItem.done === undefined ? false : updatedItem.done
                            });
                        }
                    }

                    list.items = list.items.filter(item => !itemsToRemove.some(itemToRemove => itemToRemove.id === item.id));
                }
            }
        }
    }

    @Mutation
    addList(list: todo.PartialTodoList) {
        this.lists.push(todo.EditableTodoList.from(list).toPlainObject());
    }

    @Mutation
    removeList(listToRemove: todo.PartialTodoList) {
        this.lists = this.lists.filter(list => list !== listToRemove && list.id !== listToRemove);
    }

    private static generateNewId(isIdTaken: (id: string) => boolean): string {
        let newId = 0;

        do {
            newId++;
        } while (isIdTaken(newId.toString()));

        return newId.toString();
    }
}