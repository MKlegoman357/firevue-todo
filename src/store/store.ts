import Vue from "vue";
import Vuex, {Store} from "vuex";

import TodosModule, {TodosState} from "@/store/modules/todos";
import FirestoreModule, {FirestoreModuleState} from "@/store/modules/firestore";
import VuexDecoratorsPlugin from "@/plugins/vuex/vuex-decorators/vuex-decorators";
import FooModule, {FooModuleState} from "@/store/modules/foo";

Vue.use(Vuex);

export type StoreState = {
    todos: TodosState,
    firestore: FirestoreModuleState,
    foo: FooModuleState
}

export const todosModule = new TodosModule();
export const firestoreModule = new FirestoreModule();
export const fooModule = new FooModule();

const store: Store<StoreState> = new Vuex.Store({
    strict: process.env.NODE_ENV !== "production",
    modules: {
        todos: todosModule.getModule(),
        firestore: firestoreModule.getModule(),
        foo: fooModule.getModule()
    },
    plugins: [
        VuexDecoratorsPlugin
    ]
});

export default store;